#!/bin/bash

ServerName="${SERVER_NAME:-QuakeLive}"
Password="${PASSWORD:-}"
AdminPassword="${ADMIN_PASSWORD:-}"
StatsPassword="${STATS_PASSWORD:-}"

steamcmd +login anonymous +force_install_dir /game +app_update 349090 validate +exit

/game/run_server_x64.sh \
+set net_strict 1 \
+set net_port 27960 \
+set sv_hostname "${ServerName}" \
+set sv_tags "${ServerName}" \
+set sv_servertype 2 \
+set sv_master 1 \
+set g_password "${Password}" \
+set zmq_rcon_enable 1 \
+set zmq_rcon_password "${AdminPassword}" \
+set zmq_rcon_port 28960 \
+set zmq_stats_enable 1 \
+set zmq_stats_password "${StatsPassword}" \
+set zmq_stats_port 27960 \
+set sv_vac 0