# QuakeLiveDocker

Dockerised Quake Live server.

Run with the following environment variables:

```
HOSTNAME       (default: Quake)
PASSWORD       (default: none)
ADMIN_PASSWORD (default: none)
STATS_PASSWORD (default: none)
```

Example `docker-compose.yml` file:

```yml
version: '3'
services:
  quakelive:
    image: docker.nexus.jacknet.io/jackhadrill/games/quakelive:latest
    ports:
      - '27960:27960/udp'
      - '27960:27960/tcp'
      - '28960:28960/tcp'
    environment:
      HOSTNAME: 'JackNet'
      PASSWORD: 'password123'
      ADMIN_PASSWORD: 'superSecurePassword'
```

Mount `/game` if you want to persist the server files. Else, it will download on each launch.