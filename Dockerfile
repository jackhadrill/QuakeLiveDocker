FROM steamcmd/steamcmd:ubuntu

WORKDIR /game
COPY start.sh /start.sh

ENTRYPOINT [ "bash" ]
CMD [ "/start.sh" ]